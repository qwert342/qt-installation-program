#include "installthread.h"
#include "main.h"

InstallThread::InstallThread(QString installPath, qint64 num, QObject *parent)
    : QObject(parent)
    , size(0)
    , installPath(installPath)
{
    value = 100.0 / num;
}

void InstallThread::startCopy(QString file)
{
    QThread::msleep(30); //因为cp太快所以加个延时效果
    QString copyFile = file;
    if (copyFile.contains(xin::qrcName)) {
        copyFile = copyFile.mid(xin::nameSize);
    }
    QFileInfo fileInfo(file);

    if (fileInfo.isDir()) {
        if (!QDir().mkpath(installPath + "/" + fileInfo.baseName())) {
            emit signalErrorData("文件安装失败,请重新安装,检查路径是否正确,是否有权限!");
            return;
        }
    }
    else {
        qDebug() <<file << "-----" << installPath  + copyFile;
        //检测本地路径是否存在,不存在则创建
        if (!QFileInfo(installPath).exists()) {
            if (!QDir().mkpath(installPath)) {
                emit signalErrorData("文件安装失败,请重新安装,检查路径是否正确,是否有权限!");
                return;
            }
        }
        if (QFileInfo(installPath + copyFile).exists()) { //如果存在覆盖掉
            QDir().remove(installPath + copyFile);
        }
        if (QFile().copy(file, installPath + copyFile)) {
            QFile().setPermissions(installPath + copyFile, QFile::WriteOther); //修改权限
        }
        else {
            emit signalErrorData("文件安装失败,请重新安装,检查路径是否正确,是否有权限!");
            return;
        }
    }
    emit signalData(size += value);
}
