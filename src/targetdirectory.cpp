#include "targetdirectory.h"
#include "ui_targetdirectory.h"

TargetDirectory::TargetDirectory(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TargetDirectory),
    performInstallation(nullptr),
    installPath(""),
    filesSize(0)
{
    ui->setupUi(this);

    this->setWindowIcon(QIcon(":/resourceFile/xin.ico"));
    this->setWindowTitle(tr("%1 %2 安装").arg(xin::exeName).arg(xin::versions));
    ui->labelIco->setPixmap(QPixmap(":/png/xin.ico"));
    ui->labelIco->setScaledContents(true); //自适应
    ui->labelSelectInstall->setText(tr("选择安装位置"));
    ui->labelSelectDir->setText(tr("选择 \"%1 %2 \"的安装文件夹.").arg(xin::exeName).arg(xin::versions));
    ui->labelTxt1->setText(tr("安装程序将吧 %1 %2 安装在下面的文件夹中.").arg(xin::exeName).arg(xin::versions));
    ui->labelTxt2->setWordWrap(true); //自动换行
    ui->labelTxt2->setText(tr("继续安装,请点击 [安装]. 如果需要安装到其他的目录,请单击 [浏览]"));
    ui->labelSelectInstall->setFont(QFont("Times", 10, QFont::Bold));

    ui->groupBoxDestinationFolder->setTitle(tr("目标文件夹"));

    ui->pushButtonBrowse->setText(tr("浏览(B)"));
    ui->pushButtonBrowse->setShortcut(Qt::Key_B);
    ui->pushButtonInstall->setText(tr("安装(I)"));
    ui->pushButtonBrowse->setShortcut(Qt::Key_I);
    ui->pushButtonCancel->setText(tr("取消(C)"));
    ui->pushButtonBrowse->setShortcut(Qt::Key_C);
    ui->pushButtonLastStep->setText(tr("< 上一步(B)"));
    ui->pushButtonBrowse->setShortcut(Qt::Key_B);

    connect(ui->pushButtonBrowse, SIGNAL(clicked(bool)), this, SLOT(buttonBrowse()));
    connect(ui->pushButtonInstall, SIGNAL(clicked(bool)), this, SLOT(buttonInstall()));
    connect(ui->pushButtonCancel, SIGNAL(clicked(bool)), this, SLOT(buttonCancel()));
    connect(ui->pushButtonLastStep, SIGNAL(clicked(bool)), this, SLOT(buttonLastStep()));

    connect(ui->lineEditDir, SIGNAL(textChanged(QString)), this, SLOT(textChanged(QString)));

    filesSize = getFilesSize(xin::qrcName);
    initTargetDirectory();
}

TargetDirectory::~TargetDirectory()
{
    delete ui;
}

void TargetDirectory::closeEvent(QCloseEvent *event)
{
    QMessageBox* box = new QMessageBox(QMessageBox::Question, tr("%1 %2的安装").arg(xin::exeName).arg(xin::versions) ,
                                       tr("你确定要退出%1 %2 的安装吗?").arg(xin::exeName).arg(xin::versions),
                                       QMessageBox::Yes | QMessageBox::No);

    box->button(QMessageBox::Yes)->setText(tr("是"));
    box->button(QMessageBox::No)->setText(tr("否"));
    box->button(QMessageBox::No)->setFocus(); //设置焦点
    //此move会有变动
    box->move(this->x() + (((this->width() - 310) / 2)), this->y() + ((this->height() - 120) / 2));
    int ret = box->exec();
    if (ret != QMessageBox::Yes) {
        event->ignore();
    }
}

void TargetDirectory::mssageBoxShow(QString text)
{
    QMessageBox msgBox;
    msgBox.setText(text);
    //此move会有变动
    msgBox.move(this->x() + (((this->width() - 200) / 2)), this->y() + ((this->height() - 120) / 2));
    msgBox.exec();
}

void TargetDirectory::setMoveWindows(QPoint point)
{
    this->move(point);
}

void TargetDirectory::initTargetDirectory()
{
#if defined(Q_OS_LINUX)

    QString path = QDir().homePath();
    QDir dir(path);
    QStorageInfo storage1;
    storage1.setPath(path);
    if (dir.exists()) {
        ui->lineEditDir->setText(dir.path() + "/" + xin::exeName);
        installPath = dir.path() + "/" + xin::exeName;
    }
    else {
        storage1.setPath(QDir().rootPath());
        ui->lineEditDir->setText(storage1.rootPath());
        installPath = storage1.rootPath();
    }
    ui->labelWantSpace->setText(tr("所需空间:%1").arg(bytesToSize(filesSize)));
    ui->labelSurplusSpace->setText(tr("可用空间:%1").arg(bytesToSize(storage1.bytesAvailable())));
#else
    QDir dir("C:\\Program Files (x86)");
    if (dir.exists()) {
        ui->lineEditDir->setText("C:\\Program Files (x86)\\" + xin::exeName + "\\");
        installPath = "C:\\Program Files (x86)";
        ui->lineEditDir->selectAll();
        storage.setPath(installPath);
    }
    else {
        storage = QStorageInfo::root(); //获取root目录
        ui->lineEditDir->setText(storage.rootPath());
        installPath = storage.rootPath();
        ui->lineEditDir->selectAll();
    }
    ui->labelWantSpace->setText(tr("所需空间:%1").arg(bytesToSize(filesSize)));
    ui->labelSurplusSpace->setText(tr("可用空间:%1").arg(bytesToSize(storage.bytesAvailable())));
#endif
}

QString TargetDirectory::bytesToSize(qint64 mByte)
{
    QString unit;
    double size = mByte * 1.0;
    if (size <= 0) {
        size = 0.0;
        unit = "KB";
    }
    else if (size < 1000) {
        unit = "B";
    }
    else if (size < (1024 * 1000)) {
        size /= 1024;
        unit = "KB";
    }
    else if (size < (1024 * 1024 * 1000)) {
        size /= (1024 * 1024);
        unit = "MB";
    }
    else {
        size /= (1024 * 1024 * 1024);
        unit = "GB";
    }
    return QString("%1%2").arg(QString::number(size, 'f', 2)).arg(unit);
}

qint64 TargetDirectory::getFilesSize(QString path)
{
    if (!path.endsWith('/')) {
        path.append('/');
    }
    QDir dir(path);
    qint64 size = 0;

    foreach (QString name, dir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot)) {
        QString newPath = path + name;
        if (!QFileInfo(newPath).isSymLink() && QFileInfo(newPath).isDir()) {
            getFilesSize(newPath);
        }
        size += QFileInfo(newPath).size();
    }

    return size;
}

void TargetDirectory::showTargetDirectory()
{
    this->show();
}

void TargetDirectory::buttonInstall()
{
    installPath = ui->lineEditDir->text();

    /*检查字符串是否为空*/
    if (installPath.isEmpty()) {
        mssageBoxShow(tr("安装路径为空请重新选择"));
        return;
    }

    /*获取一个存在的路径*/
    bool isInstall = false;
    QString existPath;
    for (int i = 0; i < installPath.size(); ++i) { //从头遍历，检测此路径是否在本地存在，只要有一个存在即可安装
        qDebug() << installPath.left(installPath.size() - i);
        if (QFileInfo(installPath.left(installPath.size() - i)).exists()) {
            existPath = installPath.left(installPath.size() - i); // 保存存在的路径下面好判断
            isInstall = true;
            break;
        }
    }

    /*是否可以安装*/
    if (isInstall) {
        if (QStorageInfo(existPath).bytesAvailable() <= filesSize) {//判断磁盘空间
            mssageBoxShow(tr("磁盘空间不足请重新选择"));
            return;
        }
        //追加/
        if (!installPath.endsWith('/') && !installPath.endsWith('\\')) {
            installPath.append('/');
        }
    }
    else {
        mssageBoxShow(tr("此路径无效请重新选择"));
        return;
    }

#if defined (Q_OS_LINUX)
    performInstallation = new PerformInstallation(installPath, this->frameGeometry().topLeft());
    connect(performInstallation, SIGNAL(signalShowTargetDirectory()), this, SLOT(showTargetDirectory()));
#else
    if (performInstallation == nullptr) {
        installPath.replace('\\', '/');
        qDebug() <<installPath ;
        /*检查路径是否合法*/
        QRegExp reg("[\"*?？|<>]");
        if (reg.indexIn(installPath) != -1) { //如果匹配到特殊字符
            mssageBoxShow(tr("此路径无效请重新选择"));
            return;
        }
        if (installPath.size() >= 2) { //判断第二个字符是否有:
            if (installPath.at(1) != ":") {
                mssageBoxShow(tr("此路径无效请重新选择"));
                return;
            }
        }
        performInstallation = new PerformInstallation(installPath, this->frameGeometry().topLeft());
        connect(performInstallation, SIGNAL(signalShowTargetDirectory()), this, SLOT(showTargetDirectory()));
    }
#endif
    QDesktopWidget* desktop = QApplication::desktop(); // =qApp->desktop();也可以
    performInstallation->move((desktop->width() - performInstallation->width())/2, (desktop->height() - performInstallation->height())/2);
    performInstallation->show();
    this->hide();
}

void TargetDirectory::buttonBrowse()
{
    QStorageInfo storageTemp;
    QString dirPath = QFileDialog::getExistingDirectory(this, tr("选择要安装的目录"), ".");
    qDebug() << dirPath << "----";
    if (!dirPath.isEmpty()) {
        ui->lineEditDir->setText(dirPath + "/" + xin::exeName);
        qDebug() << dirPath << "----";
        storageTemp.setPath(dirPath);
        ui->labelSurplusSpace->setText(QString(tr("可用空间:%1").arg(bytesToSize(storageTemp.bytesAvailable()))));
    }
}

void TargetDirectory::buttonCancel()
{
    QMessageBox* box = new QMessageBox(QMessageBox::Question, tr("%1 %2的安装").arg(xin::exeName).arg(xin::versions) ,
                                       tr("你确定要退出%1 %2 的安装吗?").arg(xin::exeName).arg(xin::versions),
                                       QMessageBox::Yes | QMessageBox::No);
    box->button(QMessageBox::Yes)->setText(tr("是"));
    box->button(QMessageBox::No)->setText(tr("否"));
    box->button(QMessageBox::No)->setFocus(); //设置焦点
    //此move会有变动
    box->move(this->x() + (((this->width() - 310) / 2)), this->y() + ((this->height() - 120) / 2));
    int ret = box->exec();
    if (ret == QMessageBox::Yes) {
        exit(1);
    }
}

void TargetDirectory::buttonLastStep()
{
    emit signalShowLicenseCheck(this->frameGeometry().topLeft());
    this->hide();
}

void TargetDirectory::textChanged(QString text)
{
    /*获取存在的路径*/
    bool isInstall = false;
    QString existPath;
    storage.setPath(text);
    for (int i = 0; i < text.size(); ++i) { //从头遍历，检测此路径是否在本地存在，只要有一个存在即可安装
        if (QFileInfo(text.left(text.size() - i)).exists()) {
            existPath = text.left(text.size() - i); // 保存存在的路径下面好判断
            storage.setPath(existPath);
            isInstall = true;
            break;
        }
    }
    if (isInstall) {
        ui->labelSurplusSpace->setText(tr("可用空间:%1").arg(bytesToSize(storage.bytesAvailable())));
    }
    else {
        ui->labelSurplusSpace->setText(tr("可用空间:%1").arg(0));
    }
}
